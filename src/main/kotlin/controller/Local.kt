package controller

import model.firebase.di.firebaseModule
import model.firebase.repository.FirestoreRepository
import kotlinx.coroutines.runBlocking
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.loadKoinModules
import org.koin.core.context.startKoin
import model.scraping.di.scrapingModule
import model.scraping.repository.ScrapingRepository

class Application : KoinComponent {

    private val repository: ScrapingRepository by inject()
    private val firestore: FirestoreRepository by inject()

    fun start() = runBlocking {
       val value = repository.fetchTreasureById(138)
        println(value)
    }
}

fun main() = runBlocking {
    startKoin {
        loadKoinModules(listOf(firebaseModule, scrapingModule))
    }

    val application = Application()
    application.start()
}

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)
