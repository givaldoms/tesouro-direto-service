package controller.routes.history

import controller.pojo.filter.HistoryFilter
import io.ktor.application.*
import java.text.SimpleDateFormat

fun ApplicationCall.toHistoryFilter(): HistoryFilter {

    fun String.parseToDate() = try {
        SimpleDateFormat("yyyy/MM/dd").parse(this)
    } catch (e: Exception) {
        null
    }

    val queryParam = request.queryParameters
    val startDate = queryParam[HistoryFilter::startDate.name]
    val endDate = queryParam[HistoryFilter::endDate.name]
    val page = queryParam[HistoryFilter::page.name]
    val pageSize = queryParam[HistoryFilter::pageSize.name]

    return HistoryFilter(
        startDate = startDate?.parseToDate(),
        endDate = endDate?.parseToDate(),
        page = page?.toIntOrNull(),
        pageSize = pageSize?.toIntOrNull()
    )
}