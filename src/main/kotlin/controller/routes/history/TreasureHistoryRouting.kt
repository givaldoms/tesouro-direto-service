package controller.routes.history

import controller.routes.TREASURE_ID_PATH
import controller.routes.respondBadRequest
import controller.routes.respondNotFound
import controller.routes.respondOk
import model.firebase.repository.FirestoreRepository
import io.ktor.application.*
import io.ktor.routing.*
import org.koin.ktor.ext.inject

fun Route.treasureHistoryRouting() {
    val firestore: FirestoreRepository by inject()

    get("/treasure/history/{$TREASURE_ID_PATH}") {
        val filter = this.call.toHistoryFilter()
        val treasureId = call.parameters[TREASURE_ID_PATH]?.toIntOrNull()

        if (treasureId == null) {
            respondBadRequest("")
            return@get
        }

        val result = firestore.getTreasureHistoryById(treasureId, filter)
        if (result.isEmpty()) {
            respondNotFound()
            return@get
        }

        respondOk(result)
    }
}