package controller.routes

import controller.routes.history.treasureHistoryRouting
import controller.routes.market.marketRouting
import controller.routes.scraping.scrapingRouting
import controller.routes.treasure.treasureFilterRouting
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*
import io.ktor.util.pipeline.*

const val TREASURE_ID_PATH = "treasureId"

suspend fun PipelineContext<*, ApplicationCall>.respondUnavailable() =
    call.respondCors(HttpStatusCode.ServiceUnavailable, "Treasure bonds is unavailable")

suspend fun PipelineContext<*, ApplicationCall>.respondBadRequest(message: String) =
    call.respondCors(HttpStatusCode.BadRequest, message)

suspend fun PipelineContext<*, ApplicationCall>.respondNotFound() =
    call.respondCors(HttpStatusCode.NotFound, "Dado não encontrado")

suspend inline fun <reified T: Any> PipelineContext<*, ApplicationCall>.respondCreated(data: T) =
    call.respondCors(HttpStatusCode.Created, data)

suspend inline fun <reified T: Any> PipelineContext<*, ApplicationCall>.respondOk(data: T) =
    call.respondCors(HttpStatusCode.OK, data)

suspend inline fun <reified T: Any> ApplicationCall.respondCors(status: HttpStatusCode, message: T) {
    response.headers.append("Access-Control-Allow-Origin", "*")
    response.headers.append("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    respond(status, message)
}
fun Application.registerTreasureBondsRoutes() {
    routing {
        scrapingRouting()
        treasureFilterRouting()
        treasureHistoryRouting()
        marketRouting()
    }
}