package controller.routes.scraping

import controller.routes.respondCreated
import controller.routes.respondOk
import controller.routes.respondUnavailable
import io.ktor.routing.*
import model.firebase.repository.FirestoreRepository
import model.scraping.repository.ScrapingRepository
import org.koin.ktor.ext.inject

fun Route.scrapingRouting() {
    val scraping: ScrapingRepository by inject()
    val firestore: FirestoreRepository by inject()

    route("/treasure-bonds") {
        put {
            scraping.fetchTreasureBonds()?.let {
                firestore.saveMarket(it)
                respondCreated(it)
            } ?: respondUnavailable()
        }

        get {
            scraping.fetchTreasureBonds()?.let {
                respondOk(it)
            } ?: respondUnavailable()
        }
    }
}