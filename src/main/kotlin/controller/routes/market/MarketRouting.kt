package controller.routes.market

import controller.routes.respondOk
import controller.routes.respondUnavailable
import io.ktor.routing.*
import org.koin.ktor.ext.inject
import model.scraping.repository.ScrapingRepository

fun Route.marketRouting() {
    val scraping: ScrapingRepository by inject()

    get("/market") {
        scraping.fetchTreasureBonds()?.let {
            respondOk(TreasureMarketResponse(it))
        } ?: respondUnavailable()
    }

}