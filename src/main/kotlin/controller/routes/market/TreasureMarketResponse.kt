package controller.routes.market

import controller.pojo.MarketStatus
import controller.pojo.TreasureMarket
import kotlinx.serialization.Serializable

@Serializable
class TreasureMarketResponse(
    private val queryDate: String,
    private val openingDate: String,
    private val lastUpdateDate: String,
    private val status: MarketStatus,
) {

    constructor(treasureMarket: TreasureMarket): this(
        queryDate = treasureMarket.queryDate,
        openingDate = treasureMarket.openingDate,
        lastUpdateDate = treasureMarket.lastUpdateDate,
        status = treasureMarket.status
    )

}
