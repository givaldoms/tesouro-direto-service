package controller.routes.treasure

import controller.routes.*
import io.ktor.application.*
import io.ktor.routing.*
import org.koin.ktor.ext.inject
import model.scraping.repository.ScrapingRepository

fun Route.treasureFilterRouting() {

    val scraping: ScrapingRepository by inject()

    route("/treasure") {
        get {
            val filter = call.toTreasureFilter()
            scraping.fetchTreasureBonds(filter)?.let {
                respondOk(it)
            } ?: respondUnavailable()
        }

        get("/{$TREASURE_ID_PATH}") {
            val treasureId = call.parameters[TREASURE_ID_PATH]?.toIntOrNull()

            if (treasureId == null) {
                respondBadRequest("")
                return@get
            }
            scraping.fetchTreasureById(treasureId)?.let {
                respondOk(it)
            } ?: respondNotFound()
        }
    }

}