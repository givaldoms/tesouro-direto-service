package controller.routes.treasure

import controller.pojo.Indexer
import controller.pojo.filter.TreasureFilter
import io.ktor.application.*

fun ApplicationCall.toTreasureFilter(): TreasureFilter {
    val queryParam = request.queryParameters

    val minRedden = queryParam[TreasureFilter::minReddenPrice.name]
    val maxRedden = queryParam[TreasureFilter::maxReddenPrice.name]
    val minPurchase = queryParam[TreasureFilter::minPurchasePrice.name]
    val maxPurchase = queryParam[TreasureFilter::maxPurchasePrice.name]
    val name = queryParam[TreasureFilter::name.name]
    val indexer = queryParam[TreasureFilter::indexer.name]
    val isAvailable = queryParam[TreasureFilter::isPurchaseAvailable.name]
    val minRate = queryParam[TreasureFilter::minRate.name]

    return TreasureFilter(
        minReddenPrice = minRedden?.toDoubleOrNull(),
        maxReddenPrice = maxRedden?.toDoubleOrNull(),
        minPurchasePrice = minPurchase?.toDoubleOrNull(),
        maxPurchasePrice = maxPurchase?.toDoubleOrNull(),
        name = name,
        indexer = indexer?.let { Indexer.values().firstOrNull {it.name.equals(indexer, ignoreCase = true)} },
        isPurchaseAvailable = isAvailable?.toBooleanStrictOrNull(),
        minRate = minRate?.toDoubleOrNull(),
    )
}