package controller.pojo

import kotlinx.serialization.Serializable

@Serializable
data class TreasureHistory(
    val queryDate: String,
    val openingDate: String,
    val lastUpdateDate: String,
    val status: MarketStatus,
    val treasureBond: TreasureBond
)