package controller.pojo

import kotlinx.serialization.Serializable

@Serializable
enum class MarketStatus(val code: Int) {
    OPEN(1), CLOSED(4), UNKNOWN(2);

    companion object {
        fun fromCode(code: Int) = values().firstOrNull { it.code == code } ?: UNKNOWN
    }
}