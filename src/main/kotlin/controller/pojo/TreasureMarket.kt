package controller.pojo

import kotlinx.serialization.Serializable

@Serializable
data class TreasureMarket(
    val queryDate: String,
    val openingDate: String,
    val lastUpdateDate: String,
    val status: MarketStatus,
    val treasureBondList: List<TreasureBond>
)