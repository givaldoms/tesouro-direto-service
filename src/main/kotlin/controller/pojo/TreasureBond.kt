package controller.pojo

import kotlinx.serialization.Serializable

@Serializable
data class TreasureBond(
    val id: Int,
    val name: String,
    val title: String,
    val description: String,
    val indicationMessage: String,
    val investmentAmount: Double, //a unit amount or zero if isn't available.
    val minInvestmentAmount: Double, //min amount that can be bought or zero if isn't available
    val hasSemiannualInterest: Boolean, //if it has semiannual interest

    val annualPurchaseRate: Double, //taxa de rendimento para investir
    val annualRedemptionRate: Double, //taxa de rendimento para resgatar

    val unitPurchaseAmount: Double,//preço unitário para investir
    val unitRedemptionAmount: Double,//preço unitário para resgatar

    val indexer: Indexer,

    val maturityDate: String,

    val isAvailable: Boolean,
)