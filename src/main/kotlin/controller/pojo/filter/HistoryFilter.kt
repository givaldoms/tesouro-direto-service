package controller.pojo.filter

import java.util.Date

data class HistoryFilter(
    val startDate: Date?,
    val endDate: Date?,
    val page: Int?,
    val pageSize: Int?
)