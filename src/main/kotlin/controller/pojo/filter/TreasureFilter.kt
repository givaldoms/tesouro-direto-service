package controller.pojo.filter

import controller.pojo.Indexer

data class TreasureFilter(
    val minReddenPrice: Double?,
    val maxReddenPrice: Double?,
    val minPurchasePrice: Double?,
    val maxPurchasePrice: Double?,
    val name: String?,
    val indexer: Indexer?,
    val isPurchaseAvailable: Boolean?,
    val minRate: Double?
)
