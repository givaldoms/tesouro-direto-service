package controller.pojo

import kotlinx.serialization.Serializable

@Serializable
enum class Indexer(val code: Int) {
    IPCA(22), SELIC(17), PRE(19), IGPM(1);

    companion object {
        fun fromCode(code: Int) = values().first { it.code == code }
    }
}