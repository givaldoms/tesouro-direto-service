package controller

import controller.routes.registerTreasureBondsRoutes
import model.firebase.di.firebaseModule
import io.ktor.application.*
import io.ktor.application.Application
import io.ktor.features.*
import io.ktor.serialization.*
import io.ktor.server.netty.*
import org.koin.ktor.ext.Koin
import model.scraping.di.scrapingModule

fun main(args: Array<String>): Unit = EngineMain.main(args)

fun Application.module() {

    install(Koin) {
        modules(listOf(firebaseModule, scrapingModule))
    }

    install(ContentNegotiation) {
        json()
    }

    registerTreasureBondsRoutes()
}


