package model.scraping.service

import retrofit2.Response
import retrofit2.http.GET
import model.scraping.response.ScrapingResponse

interface TreasureBondsScrapingService {

    @GET(TREASURE_BONDS_PATH)
    suspend fun fetchBondsList(): Response<ScrapingResponse>

    companion object {
        private const val TREASURE_BONDS_PATH = "treasurybondsinfo.json"
    }
}
