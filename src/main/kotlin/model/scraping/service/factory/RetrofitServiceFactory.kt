package model.scraping.service.factory

import com.jakewharton.retrofit2.converter.kotlinx.serialization.asConverterFactory
import kotlinx.serialization.json.Json
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.create
import model.scraping.service.TreasureBondsScrapingService

class RetrofitServiceFactory(
    private val okHttpClient: OkHttpClient
) : ServiceFactory<TreasureBondsScrapingService>() {

    private val jsonMediaType = "application/json".toMediaTypeOrNull()!!

    @Suppress("EXPERIMENTAL_API_USAGE")
    override fun createInstance() = Retrofit.Builder()
        .baseUrl(TREASURE_BONDS_BASE_URL)
        .addConverterFactory(Json.asConverterFactory(jsonMediaType))
        .client(okHttpClient)
        .build()
        .create<TreasureBondsScrapingService>()

    companion object {
        private const val TREASURE_BONDS_BASE_URL =
            "http://www.tesourodireto.com.br/json/br/com/b3/tesourodireto/service/api/"
    }
}