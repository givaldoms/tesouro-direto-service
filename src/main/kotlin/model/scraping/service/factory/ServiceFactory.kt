package model.scraping.service.factory

abstract class ServiceFactory<out T> {

    abstract fun createInstance(): T

}