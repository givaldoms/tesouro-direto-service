package model.scraping.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TreasureListResponse (
	@SerialName("TrsrBd") val Treasure : TreasureResponse,
	@SerialName("TrsrBdType") val treasureType : TreasureTypeResponse,
)