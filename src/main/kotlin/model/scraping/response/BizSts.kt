package model.scraping.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BizSts (
	@SerialName("cd") val code : Int,
	@SerialName("dtTm") val dateTime : String
)