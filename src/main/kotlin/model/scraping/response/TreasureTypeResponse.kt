package model.scraping.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TreasureTypeResponse(
    @SerialName("cd") val code: Int,
    @SerialName("nm") val name: String,
    @SerialName("ctdyRate") val rate: Float,
    @SerialName("grPr") val price: Int,
)