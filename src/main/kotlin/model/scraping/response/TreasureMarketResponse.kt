package model.scraping.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TreasureMarketResponse (
	@SerialName("opngDtTm") val openingDateTime : String,
	@SerialName("clsgDtTm") val closingDateTime : String,
	@SerialName("qtnDtTm") val lastUpdateDateTime : String,
	@SerialName("stsCd") val statusCode : Int,
	@SerialName("sts") val status : String
)