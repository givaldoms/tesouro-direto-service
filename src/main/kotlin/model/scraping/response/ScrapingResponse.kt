package model.scraping.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ScrapingResponse (
	@SerialName("responseStatus") val responseStatus : Int,
	@SerialName("responseStatusText") val responseStatusText : String,
	@SerialName("statusInfo") val statusInfo : String,
	@SerialName("response") val data : ScrapingDataResponse
)