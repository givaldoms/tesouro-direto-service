package model.scraping.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class BdTxTp(
    @SerialName("cd") val cd: Int
)