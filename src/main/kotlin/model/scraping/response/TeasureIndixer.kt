package model.scraping.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TreasureIndexer (
	@SerialName("cd") val code : Int,
	@SerialName("nm") val name : String
)