package model.scraping.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class ScrapingDataResponse (
    @SerialName("BdTxTp") val bdTxTp : BdTxTp,//identificar
    @SerialName("TrsrBondMkt") val market : TreasureMarketResponse,//status do mercado
    @SerialName("TrsrBdTradgList") val treasureList : List<TreasureListResponse>, // lista de títulos
    @SerialName("BizSts") val query : BizSts//informações que identifica a consulta
)