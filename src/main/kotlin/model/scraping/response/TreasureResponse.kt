package model.scraping.response

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
data class TreasureResponse(
    @SerialName("cd") val code: Int,
    @SerialName("nm") val name: String,
    @SerialName("featrs") val title: String,
    @SerialName("invstmtStbl") val description: String,
    @SerialName("rcvgIncm") val indicationMessage: String,
    @SerialName("minInvstmtAmt") val minInvestmentAmount: Double,
    @SerialName("untrInvstmtVal") val unitInvestmentAmount: Double,
    @SerialName("semiAnulIntrstInd") val autoInterest: Boolean,

    @SerialName("anulInvstmtRate") val annualRate: Double,
    @SerialName("anulRedRate") val anulRedRate: Double,

    @SerialName("minRedQty") val minTaxAmount: Double,//valor minimo que pode ser comprado. E sempre 0.01 (1%)
    @SerialName("untrRedVal") val unitPriceAmount: Double,
    @SerialName("minRedVal") val minAmount: Double, //unitPrice * minTaxAmount
    @SerialName("isinCd") val isinCode: String,//algum código usado internamente
    @SerialName("FinIndxs") val indexer: TreasureIndexer,//ipca, selic, prefixido, igp-m
    @SerialName("mtrtyDt") val maturityDate: String,
    @SerialName("wdwlDt") val wdwlDt: String? = null//suspeito que seja dia de abertura
)