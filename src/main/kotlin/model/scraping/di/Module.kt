package model.scraping.di

import org.koin.dsl.module
import model.scraping.repository.ScrapingRepository
import model.scraping.repository.ScrapingRepositoryImpl
import model.scraping.service.factory.OkhttpClientFactory
import model.scraping.service.factory.RetrofitServiceFactory

val scrapingModule = module {

    factory {
        RetrofitServiceFactory(okHttpClient = get()).createInstance()
    }

    factory {
        OkhttpClientFactory().createInstance()
    }

    factory<ScrapingRepository> {
        ScrapingRepositoryImpl(service = get())
    }
}