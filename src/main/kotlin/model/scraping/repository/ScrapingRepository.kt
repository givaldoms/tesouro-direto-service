package model.scraping.repository

import controller.pojo.TreasureBond
import controller.pojo.TreasureMarket
import controller.pojo.filter.TreasureFilter

interface ScrapingRepository {

    suspend fun fetchTreasureBonds(): TreasureMarket?

    suspend fun fetchTreasureById(treasureId: Int): TreasureBond?

    suspend fun fetchTreasureBonds(filter: TreasureFilter): List<TreasureBond>?
}