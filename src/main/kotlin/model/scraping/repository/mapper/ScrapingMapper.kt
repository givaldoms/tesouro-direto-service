package model.scraping.repository.mapper

import controller.pojo.Indexer
import controller.pojo.MarketStatus
import controller.pojo.TreasureBond
import controller.pojo.TreasureMarket
import model.scraping.response.ScrapingResponse
import model.scraping.response.TreasureResponse

object ScrapingMapper {
    fun ScrapingResponse.toDomain(): TreasureMarket {
        val market = data.market
        val query = data.query
        val treasureList = data.treasureList

        return TreasureMarket(
            queryDate = query.dateTime,
            openingDate = market.openingDateTime,
            lastUpdateDate = market.lastUpdateDateTime,
            status = MarketStatus.fromCode(market.statusCode),
            treasureBondList = treasureList.map { it.Treasure.toDomain() }
        )
    }

    private fun TreasureResponse.toDomain(): TreasureBond {
        val isInvestmentAvailable = unitInvestmentAmount > 0

        return TreasureBond(
            id = code,
            name = name,
            title = title,
            description = description,
            indicationMessage = indicationMessage,
            investmentAmount = unitInvestmentAmount,
            minInvestmentAmount = minInvestmentAmount,
            hasSemiannualInterest = autoInterest,
            annualPurchaseRate = annualRate,
            annualRedemptionRate = anulRedRate,
            unitPurchaseAmount = unitInvestmentAmount,
            unitRedemptionAmount = unitPriceAmount,
            maturityDate = maturityDate,
            isAvailable = isInvestmentAvailable,
            indexer = Indexer.fromCode(indexer.code)
        )
    }
}