package model.scraping.repository

import controller.pojo.TreasureBond
import controller.pojo.TreasureMarket
import controller.pojo.filter.TreasureFilter
import model.scraping.repository.mapper.ScrapingMapper.toDomain
import model.scraping.service.TreasureBondsScrapingService

class ScrapingRepositoryImpl(
    private val service: TreasureBondsScrapingService
) : ScrapingRepository {

    override suspend fun fetchTreasureBonds(): TreasureMarket? {
        return service.fetchBondsList().body()?.toDomain()
    }

    override suspend fun fetchTreasureById(treasureId: Int): TreasureBond? {
        return fetchTreasureBonds()?.treasureBondList?.filter {
            it.id == treasureId
        }?.firstOrNull()
    }

    override suspend fun fetchTreasureBonds(filter: TreasureFilter): List<TreasureBond>? {
        return fetchTreasureBonds()?.treasureBondList
            ?.filter {
                (filter.indexer == null || filter.indexer == it.indexer)
                        && (filter.name == null || it.name.contains(filter.name, true))
                        && (filter.isPurchaseAvailable == null || filter.isPurchaseAvailable == it.isAvailable)
                        && (filter.minReddenPrice == null || filter.minReddenPrice >= it.unitRedemptionAmount)
                        && (filter.maxReddenPrice == null || filter.maxReddenPrice <= it.unitRedemptionAmount)
                        && (filter.minPurchasePrice == null || it.minInvestmentAmount >= filter.minPurchasePrice)
                        && (filter.maxPurchasePrice == null || it.minInvestmentAmount <= filter.maxPurchasePrice)
                        && ((filter.minRate == null || it.annualRedemptionRate == 0.0 || filter.minRate >= it.annualRedemptionRate)
                        || (it.annualPurchaseRate == 0.0 || filter.minRate >= it.annualPurchaseRate))

            }
    }
}