package model.firebase.factory

import com.google.cloud.firestore.Firestore

interface FirebaseInitializer {

    val firestore: Firestore

    fun initialize()

}