package model.firebase.factory

import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.firestore.Firestore
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import com.google.firebase.cloud.FirestoreClient
import java.io.FileInputStream

class FirebaseInitializerImpl : FirebaseInitializer {

    override val firestore: Firestore by lazy {
        initialize()
        FirestoreClient.getFirestore()
    }

    private val firebaseKey = FileInputStream("src/main/resource/firebase-key.json")

    override fun initialize() {
        try {
            FirebaseApp.getInstance()
        } catch (ex: Exception) {
            FirebaseApp.initializeApp(createOptions())
        }
    }

    private fun createOptions(): FirebaseOptions {
        val credentials = GoogleCredentials.fromStream(firebaseKey)

        return FirebaseOptions.builder()
            .setCredentials(credentials)
            .build()
    }
}