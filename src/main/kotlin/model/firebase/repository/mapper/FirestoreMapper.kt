package model.firebase.repository.mapper

import controller.pojo.TreasureBond
import controller.pojo.TreasureMarket
import model.firebase.model.TreasureBondFirestore
import model.firebase.model.TreasureMarketFirestore
import java.text.SimpleDateFormat
import java.util.*

object FirestoreMapper {

    fun TreasureMarket.toTreasureMarketFirestore() = TreasureMarketFirestore(
        queryDate = queryDate.parseToDate(),
        openingDate = openingDate.parseToDate(),
        lastUpdateDate = lastUpdateDate.parseToDate(),
        status = status.code
    )

    fun TreasureBond.toTreasureBondFirestore() = TreasureBondFirestore(
        id = id,
        name = name,
        title = title,
        description = description,
        indicationMessage = indicationMessage,
        investmentAmount = investmentAmount,
        minInvestmentAmount = minInvestmentAmount,
        hasSemiannualInterest = hasSemiannualInterest,
        annualPurchaseRate = annualPurchaseRate,
        annualRedemptionRate = annualRedemptionRate,
        unitPurchaseAmount = unitPurchaseAmount,
        unitRedemptionAmount = unitRedemptionAmount,
        indexer = indexer.code,
        maturityDate = maturityDate.parseToDate(),
        isAvailable = isAvailable
    )

    fun String.parseToDate(): Date = try {
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").parse(this)
    } catch (e: Exception) {
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS").parse(this)
    }
}
