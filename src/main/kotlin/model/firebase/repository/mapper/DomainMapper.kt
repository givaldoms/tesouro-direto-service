package model.firebase.repository.mapper

import model.firebase.model.TreasureBondFirestore
import model.firebase.model.TreasureMarketFirestore
import controller.pojo.*
import java.text.SimpleDateFormat
import java.util.*

object DomainMapper {

    fun TreasureMarketFirestore.toTreasureMarket(list: List<TreasureBondFirestore>) = TreasureMarket(
        queryDate = queryDate.parseFromDate(),
        openingDate = openingDate.parseFromDate(),
        lastUpdateDate = lastUpdateDate.parseFromDate(),
        status = MarketStatus.fromCode(status),
        treasureBondList = list.map { it.toTreasureBond() }
    )

    fun TreasureMarketFirestore.toTreasureHistory(treasureBond: TreasureBondFirestore) = TreasureHistory(
        queryDate = queryDate.parseFromDate(),
        openingDate = openingDate.parseFromDate(),
        lastUpdateDate = lastUpdateDate.parseFromDate(),
        status = MarketStatus.fromCode(status),
        treasureBond = treasureBond.toTreasureBond()
    )

    fun TreasureBondFirestore.toTreasureBond() = TreasureBond(
        id = id,
        name = name,
        title = title,
        description = description,
        indicationMessage = indicationMessage,
        investmentAmount = investmentAmount,
        annualPurchaseRate = annualPurchaseRate ?: 0.0,
        annualRedemptionRate = annualRedemptionRate ?: 0.0,
        unitRedemptionAmount = unitRedemptionAmount ?: 0.0,
        unitPurchaseAmount = unitPurchaseAmount ?: 0.0,
        minInvestmentAmount = minInvestmentAmount,
        hasSemiannualInterest = hasSemiannualInterest,
        indexer = Indexer.fromCode(indexer),
        maturityDate = maturityDate.parseFromDate(),
        isAvailable = isAvailable
    )

    private fun Date.parseFromDate(): String = try {
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss").format(this)
    } catch (e: Exception) {
        SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SS").format(this)
    }

}