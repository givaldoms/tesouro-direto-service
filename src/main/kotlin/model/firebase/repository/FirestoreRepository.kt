package model.firebase.repository

import controller.pojo.TreasureHistory
import controller.pojo.TreasureMarket
import controller.pojo.filter.HistoryFilter

interface FirestoreRepository {

    suspend fun saveMarket(market: TreasureMarket)

    suspend fun getTreasureHistoryById(treasureId: Int, historyFilter: HistoryFilter): List<TreasureHistory>

}