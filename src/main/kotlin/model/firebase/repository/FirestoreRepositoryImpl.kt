package model.firebase.repository

import com.google.cloud.firestore.Query.Direction.DESCENDING
import controller.pojo.TreasureHistory
import controller.pojo.TreasureMarket
import controller.pojo.filter.HistoryFilter
import model.firebase.factory.FirebaseInitializer
import model.firebase.model.TreasureBondFirestore
import model.firebase.model.TreasureMarketFirestore
import model.firebase.repository.mapper.DomainMapper.toTreasureHistory
import model.firebase.repository.mapper.FirestoreMapper.parseToDate
import model.firebase.repository.mapper.FirestoreMapper.toTreasureBondFirestore
import model.firebase.repository.mapper.FirestoreMapper.toTreasureMarketFirestore

@Suppress("BlockingMethodInNonBlockingContext")
class FirestoreRepositoryImpl(
    private val firebase: FirebaseInitializer,
) : FirestoreRepository {

    private val marketCollection by lazy {
        firebase.firestore.collection(MARKET_COLLECTION_PATH)
    }

    override suspend fun saveMarket(market: TreasureMarket) {
        firebase.firestore.runTransaction { transaction ->
            val marketDoc = marketCollection.document()
            val date = market.lastUpdateDate.parseToDate()
            val query = marketCollection.whereEqualTo(MARKET_LAST_UPDATED_FIELD, date).limit(1)

            val oldMarket = transaction.get(query)
            if (oldMarket.get().isEmpty.not()) return@runTransaction

            transaction.set(marketDoc, market.toTreasureMarketFirestore())
            market.treasureBondList.forEach { treasure ->
                val ref = marketCollection.document(marketDoc.id).collection(TREASURE_COLLECTION_PATH).document()
                transaction.set(ref, treasure.toTreasureBondFirestore())
            }
        }
    }

    override suspend fun getTreasureHistoryById(treasureId: Int, historyFilter: HistoryFilter): List<TreasureHistory> {
        val count = historyFilter.pageSize ?: 3
        val offset = historyFilter.page?.times(count) ?: 0

        val marketQuery = marketCollection.orderBy(MARKET_LAST_UPDATED_FIELD, DESCENDING)
            .offset(offset)
            .limit(count)

        if (historyFilter.startDate != null) {
            marketQuery.whereGreaterThanOrEqualTo(MARKET_LAST_UPDATED_FIELD, historyFilter.startDate)
        }

        if (historyFilter.endDate != null) {
            marketQuery.whereLessThanOrEqualTo(MARKET_LAST_UPDATED_FIELD, historyFilter.endDate.time)
        }

        val marketList = marketQuery.get().get().map { it.id to it.toObject(TreasureMarketFirestore::class.java) }

        return marketList.mapNotNull { marketPair ->
            val treasure = marketCollection.document(marketPair.first).collection(TREASURE_COLLECTION_PATH)
                .whereEqualTo(TREASURE_ID_FIELD, treasureId)
                .get().get().toObjects(TreasureBondFirestore::class.java)

            treasure.takeIf { treasure.isNotEmpty() }?.let {
                marketPair.second.toTreasureHistory(it.first())
            }

        }
    }

    companion object {
        private const val MARKET_COLLECTION_PATH = "market"
        private const val TREASURE_COLLECTION_PATH = "treasure"

        private const val MARKET_LAST_UPDATED_FIELD = "lastUpdateDate"
        private const val TREASURE_ID_FIELD = "id"
    }
}