package model.firebase.model

import java.util.Date

data class TreasureMarketFirestore(
    var queryDate: Date,
    var openingDate: Date,
    var lastUpdateDate: Date,
    var status: Int
) {
    constructor() : this(Date(), Date(), Date(), -1)
}