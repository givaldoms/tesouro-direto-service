package model.firebase.model

import java.util.Date

data class TreasureBondFirestore(
    var id: Int,
    var name: String,
    var title: String,
    var description: String,
    var indicationMessage: String,
    var investmentAmount: Double, //a unit amount or zero if isn't available.
    var minInvestmentAmount: Double, //min amount that can be bought or zero if isn't available
    var hasSemiannualInterest: Boolean, //if it has semiannual interest
    var annualPurchaseRate: Double?, //taxa de rendimento para investir
    var annualRedemptionRate: Double?, //taxa de rendimento para resgatar
    var unitPurchaseAmount: Double?,//preço unitário para investir
    var unitRedemptionAmount: Double?,//preço unitário para resgatar
    var indexer: Int,
    var maturityDate: Date,
    var isAvailable: Boolean,
) {

    constructor() : this(
        id = 0,
        name = "",
        title = "",
        description = "",
        indicationMessage = "",
        investmentAmount = 0.0,
        minInvestmentAmount = 0.0,
        hasSemiannualInterest = false,
        annualPurchaseRate = 0.0,
        annualRedemptionRate = 0.0,
        unitPurchaseAmount = 0.0,
        unitRedemptionAmount = 0.0,
        indexer = 0,
        maturityDate = Date(),
        isAvailable = false,
    )
}