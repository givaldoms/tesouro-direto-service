package model.firebase.di

import model.firebase.factory.FirebaseInitializer
import model.firebase.factory.FirebaseInitializerImpl
import model.firebase.repository.FirestoreRepository
import model.firebase.repository.FirestoreRepositoryImpl
import org.koin.dsl.module

val firebaseModule = module {

    factory<FirebaseInitializer> {
        FirebaseInitializerImpl()
    }

    factory<FirestoreRepository> {
        FirestoreRepositoryImpl(
            firebase = get()
        )
    }
}